package ru.startandroid.algstruct;

import android.content.ContentValues;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    Spinner spSort;
    Button btnGenerate,btnSort;
    Random random;
    TextView tvRes,tvEndSort;
    boolean bubbleSort = false, shellSort = false, quickSort = false;
    String numbers = "";
    String sorted;
    long time_before_sort,time_after_sort, delay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView gitId = findViewById(R.id.gitId);
        gitId.setText("firstProject");

        tvEndSort = (TextView) findViewById(R.id.tvEndSort);
        tvRes = (TextView) findViewById(R.id.tvRes);

        spSort = (Spinner) findViewById(R.id.spSort);
        spSort.setPrompt("Обрати вид сортування");

        btnGenerate = (Button) findViewById(R.id.btnGenerate);
        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                random= new Random();
                for(int i = 0; i < 200; i++){
                    numbers += String.valueOf(random.nextInt(100)) + " ";
                }

                tvRes.setText("Масив випадкових чисел: " + numbers);
                btnSort.setEnabled(true);

            }
        });
        btnSort = (Button) findViewById(R.id.btnSort);
        btnSort.setEnabled(false);
        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(this, R.array.sortList, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSort.setAdapter(adapter);

        spSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

                String[] choose = getResources().getStringArray(R.array.sortList);
                if(choose[selectedItemPosition].equals("Бульбашкове сортування") ){ bubbleSort = true;}
                else if(choose[selectedItemPosition].equals("Сортування Шелла")){shellSort = true;}
                else if(choose[selectedItemPosition].equals("Швидке сортування")){quickSort = true;}
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        btnSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strArr[] = numbers.split(" ");
                int numArr[] = new int[strArr.length];
                for (int i = 0; i < strArr.length; i++) {
                    numArr[i] = Integer.parseInt(strArr[i]);
                }
                if(shellSort){

                    Calendar before = Calendar.getInstance();
                    time_before_sort = before.getTimeInMillis();
                    shSort(numArr);
                    Calendar after = Calendar.getInstance();
                    time_after_sort = after.getTimeInMillis();
                    delay = time_after_sort - time_before_sort;
                    sorted = Arrays.toString(numArr);
                    tvEndSort.setText("Масив відсортовано сортуванням Шелла за " + delay+" мілісекунд" + sorted);
                    shellSort = false;
                }
                else if(quickSort)
                {
                    Calendar before = Calendar.getInstance();
                    time_before_sort = before.getTimeInMillis();
                    shSort(numArr);
                    Calendar after = Calendar.getInstance();
                    time_after_sort = after.getTimeInMillis();
                    delay = time_after_sort - time_before_sort;
                    sorted = Arrays.toString(numArr);
                    tvEndSort.setText("Масив вісортовано швидким сортуванням за " + delay + " мілісекунд" + sorted);
                    quickSort = false;
                }
                else if(bubbleSort){
                    Calendar before = Calendar.getInstance();
                    time_before_sort = before.getTimeInMillis();
                    bubbleSort(numArr);
                    Calendar after = Calendar.getInstance();
                    time_after_sort = after.getTimeInMillis();
                    delay = time_after_sort - time_before_sort;
                    sorted = Arrays.toString(numArr);
                    tvEndSort.setText("Масив вісортовано бульбашковим сортуванням за " + delay + " мілісекунд" + sorted);
                    bubbleSort = false;
                }

            }
        });


    }
    public static void shSort(int  numArr[]){
        boolean changes;
        do {
            changes = false;
            for (int i = 0; i < numArr.length - 1; i++) {
                if (numArr[i] < numArr[i + 1]) {
                    int _current = numArr[i];
                    numArr[i] = numArr[i + 1];
                    numArr[i + 1] = _current;
                    changes = true;
                }
            }

        }
        while (changes);


    }

    public static void quickSorting(int[] numArr) {
        boolean isSorted = false;

        while (!isSorted) {
            isSorted = true;

            for (int i = 0; i < numArr.length - 1; i += 2) {
                if (numArr[i] > numArr[i + 1]) {
                    int temp = numArr[i];
                    numArr[i] = numArr[i + 1];
                    numArr[i + 1] = temp;
                    isSorted = false;
                }
            }
            for (int i = 1; i < numArr.length - 1; i += 2) {
                if (numArr[i] > numArr[i + 1]) {
                    int temp = numArr[i];
                    numArr[i] = numArr[i + 1];
                    numArr[i + 1] = temp;
                    isSorted = false;
                }
            }
        }
    }
    public static void bubbleSort(int[] numArr) {
        int j;
        boolean flag = true;   // устанавливаем наш флаг в true для первого прохода по массиву
        int temp;   // вспомогательная переменная

        while (flag) {
            flag = false;    // устанавливаем флаг в false в ожидании возможного свопа (замены местами)
            for (j = 0; j < numArr.length - 1; j++) {
                if (numArr[j] < numArr[j + 1]) { // измените на > для сортировки по возрастанию
                    temp = numArr[j];         // меняем элементы местами
                    numArr[j] = numArr[j + 1];
                    numArr[j + 1] = temp;
                    flag = true;  // true означает, что замена местами была проведена
                }
            }
        }
    }

}
